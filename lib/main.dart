import 'package:flutter/material.dart';
import 'package:kerman_city/Account.dart';
import 'package:kerman_city/Card.dart';
import 'package:kerman_city/Classes/KermanCard.dart';
import 'package:kerman_city/bus.dart';
import 'package:kerman_city/dir.dart';
import 'package:kerman_city/taxi.dart';

void main() => runApp(MaterialApp(
      title: "Transportation",
      debugShowCheckedModeBanner: false,
      home: MyApp(),
    ));

ThemeData appTheme = ThemeData(
    primaryColor: Colors.brown, accentColor: Colors.brown.withOpacity(.8));

class MyApp extends StatefulWidget {
  MyApp({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int sel = 0;

  List<BottomNavigationBarItem> Items = [
    BottomNavigationBarItem(
        icon: Icon(Icons.credit_card), title: Text("Kerman Card")),
    BottomNavigationBarItem(
        icon: Icon(Icons.local_taxi), title: Text("Taxi Pay")),
    BottomNavigationBarItem(
        icon: Icon(Icons.directions_bus), title: Text("Bus Pay")),
    BottomNavigationBarItem(
        icon: Icon(Icons.directions), title: Text("Directions")),
    BottomNavigationBarItem(
        icon: Icon(Icons.account_box), title: Text("Account Box")),
  ];
  Widget Body() {
    switch (sel) {
      case 0:
        return card();
        break;
      case 1:
        return taxi();
        break;
      case 2:
        return bus(kCard: new KermanCard(kCardId: 547484, cash: 85000));
        break;
      case 4:
        return account();
        break;
      case 3:
        return dir();
        break;
      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Transportation"),
        backgroundColor: appTheme.primaryColor,
      ),
      drawer: drawer(),
      body: Body(),
      bottomNavigationBar: BottomNavigationBar(
        items: Items,
        currentIndex: sel,
        elevation: 1.5,
        type: BottomNavigationBarType.shifting,
        backgroundColor: appTheme.primaryColor,
        //  fixedColor: Colors.black,
        selectedItemColor: appTheme.primaryColor,
        unselectedItemColor: Colors.black,
        onTap: (selected) {
          setState(() {
            sel = selected;
          });
        },
      ),
    );
  }

  Widget drawer() {
    return new Drawer(
        child: new ListView(
      children: <Widget>[
        new DrawerHeader(
          child: Container(
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/ui.png"), fit: BoxFit.fitHeight),
                borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Colors.white70),
            // child: Text("Kerman City App"),
          ),
        ),
        new ListTile(
          title: Row(
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.settings),
                onPressed: () {
                  // Navigator.of(context).push(MaterialPageRoute<void>(
                  //     builder: (BuildContext context) {
                  //   return settings(
                  //       context: context,
                  //       is_darkMode: is_darkMode,
                  //       width: width,
                  //       is_English: is_English,
                  //       height: height);
                  // }));
                },
              ),
              Text(
                'Settings',
                style: TextStyle(fontSize: 18),
              )
            ],
          ),
          onTap: () {
            // Navigator.of(context).push(
            //     MaterialPageRoute<void>(builder: (BuildContext context) {
            //   return settings(
            //       context: context,
            //       is_darkMode: is_darkMode,
            //       width: width,
            //       is_English: is_English,
            //       height: height);
            // }));
          },
        ),
        new ListTile(
          title: Row(
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.more_horiz),
                onPressed: () {
                  // Navigator.of(context).push(MaterialPageRoute<void>(
                  //     builder: (BuildContext context) {
                  //   return About(
                  //       context: context,
                  //       is_darkMode: is_darkMode,
                  //       width: width,
                  //       is_English: is_English,
                  //       height: height);
                  // }));
                },
              ),
              Text('About', style: TextStyle(fontSize: 18))
            ],
          ),
          onTap: () {
            // Navigator.of(context).push(
            //     MaterialPageRoute<void>(builder: (BuildContext context) {
            //   return About(
            //       context: context,
            //       is_darkMode: is_darkMode,
            //       width: width,
            //       is_English: is_English,
            //       height: height);
            // }));
          },
        ),
        new ListTile(
          title: Row(
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.person),
                onPressed: () {
                  // Navigator.of(context).push(MaterialPageRoute<void>(
                  //     builder: (BuildContext context) {
                  //   return Aboutme(
                  //       context: context,
                  //       is_darkMode: is_darkMode,
                  //       width: width,
                  //       is_English: is_English,
                  //       height: height);
                  // }));
                },
              ),
              Text('Contact me', style: TextStyle(fontSize: 18))
            ],
          ),
          onTap: () {
            // Navigator.of(context).push(
            //     MaterialPageRoute<void>(builder: (BuildContext context) {
            //   return Aboutme(
            //       context: context,
            //       is_darkMode: is_darkMode,
            //       width: width,
            //       is_English: is_English,
            //       height: height);
            // }));
          },
        ),
      ],
    ));
  }
}
