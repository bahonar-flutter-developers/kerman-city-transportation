import 'package:flutter/material.dart';

import 'main.dart';

class MyButton extends StatelessWidget {
  String Title;
  IconData icon;
  MyButton({this.icon, this.Title});
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        // border:  Border.all(color: appTheme.accentColor),
        //color: appTheme.primaryColor
      ),
      child: Stack(
        children: <Widget>[
          Container(
            height: 77,
            width: 100,
            decoration: BoxDecoration(
              color: Colors
                  .transparent //Color.fromARGB(255, 214, 207, 189).withOpacity(0.00005)
              ,
              borderRadius: BorderRadius.all(Radius.circular(10)),
              //boxShadow: <BoxShadow>[BoxShadow(color: Colors.transparent)]
            ),
          ),
          Container(
            height: 77,
            width: 100,
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Icon(
                    icon,
                    color: Colors.brown //Color.fromARGB(255,39,77,77)
                    ,
                  ),
                  Center(
                    child: Text(Title,
                        style: TextStyle(
                            fontSize: 15,
                            color: appTheme.accentColor,
                            fontWeight: FontWeight.w900)),
                  )
                ],
              ),
            ),
          ),
          InkWell(
            child: Container(
              height: 77,
              width: 100,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.transparent),
            ),
            onTap: () {
              showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Center(child: Text("you've clicked on : ")),
                      content: Row(
                        children: <Widget>[
                          SizedBox(
                            width: 30,
                          ),
                          Text(Title),
                        ],
                      ),
                    );
                  });
            },
          )
        ],
      ),
    );
  }
}
