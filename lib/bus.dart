import 'package:flutter/material.dart';
import 'package:kerman_city/Classes/KermanCard.dart';

// class bus extends StatelessWidget {
//   final KermanCard kCard;
//   const bus({Key key, this.kCard}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//    }
// }
class bus extends StatefulWidget {
  final KermanCard kCard;
  const bus({Key key, this.kCard}) : super(key: key);

  @override
  _busState createState() => _busState(kCard: kCard);
}

class _busState extends State<bus> {
  final KermanCard kCard;
  _busState({this.kCard});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: 60,
        ),
        Text(
          "Your NFC Is ON ",
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ), //TODO show nfc status
        SizedBox(
          height: 20,
        ),
        Center(
          child: Container(
            height: 200,
            width: 290,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/kcard.jpg"), fit: BoxFit.fill),
                //color: Colors.black,
                borderRadius: BorderRadius.all(Radius.circular(20))),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Text(
          "Your Card Id is : " + kCard.kCardId.toString(),
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 20,
        ),
        Text(
          "Your Card Cash is : " + kCard.cash.toString() + " Rials",
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
      ],
    );
  }
}
