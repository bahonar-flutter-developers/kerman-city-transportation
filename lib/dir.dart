import 'package:flutter/material.dart';
import 'package:kerman_city/Classes/KermanCard.dart';
import 'package:kerman_city/main.dart';
// class dir extends StatelessWidget {
//   const dir({Key key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Center(child: Text("dir")),
//     );
//   }
// }
class dir extends StatefulWidget {
  dir({Key key}) : super(key: key);

  @override
  _dirState createState() => _dirState();
}

class _dirState extends State<dir> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: 150,
        width: 300,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(20)),
            color: appTheme.primaryColor.withOpacity(.8)),
        child: Center(
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(
                      "Origin",
                      style: TextStyle(fontSize: 18),
                    ),
                    // PopupMenuButton(
                    //   //itemBuilder: ,
                    //   //onSelected: (dynamic){},
                    // )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Text(
                      "Destination",
                      style: TextStyle(fontSize: 18),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
