import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:kerman_city/MyButton.dart';

import 'main.dart';

class card extends StatelessWidget {
  const card({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: 800,
        width: 400,
        child: Stack(
          children: <Widget>[
            ClipPath(
              clipper: Clipper08(),
              child: Container(
                height: 200, //400
                //color: Colors.tealAccent,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [appTheme.primaryColor, appTheme.accentColor])),
                child: Column(
                  children: <Widget>[
                    // SizedBox(
                    //   height: height / 16,
                    // ),
                    Padding(
                      padding: EdgeInsets.all(16.0),
                      child: Row(
                        children: <Widget>[
                          // Icon(
                          //   Icons.location_on,
                          //   color: Colors.white,
                          // ),
                          // SizedBox(
                          //   width: width * 0.05,
                          // ),
                          Spacer(),
                          Icon(
                            Icons.settings,
                            color: Colors.white,
                          )
                        ],
                      ),
                    ),
                    // SizedBox(
                    //   height: height / 16,
                    // ),
                    // Text(
                    //   'Where Would  \n you want to go',
                    //   style: TextStyle(
                    //     fontSize: 24.0,
                    //     color: Colors.white,
                    //   ),
                    //   textAlign: TextAlign.center,
                    // ),
                    // SizedBox(height: height * 0.0375),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 32.0),
                      child: Material(
                        elevation: 5.0,
                        borderRadius: BorderRadius.all(Radius.circular(30)),
                        // child: TextField(
                        //   //controller: c,
                        //   style: TextStyle(
                        //     fontSize: 16.0,
                        //     color: Colors.black,
                        //   ),
                        //   cursorColor: appTheme.primaryColor,
                        //   decoration: InputDecoration(
                        //       border: InputBorder.none,
                        //       contentPadding: EdgeInsets.symmetric(
                        //           horizontal: 32, vertical: 13),
                        //       suffixIcon: Material(
                        //         child: InkWell(
                        //           child: Icon(
                        //             Icons.search,
                        //             color: Colors.black,
                        //           ),
                        //         ),
                        //         elevation: 2.0,
                        //         borderRadius:
                        //             BorderRadius.all(Radius.circular(30)),
                        //       )),
                        // ),
                      ),
                    ),
                    // SizedBox(
                    //   height: height * 0.025,
                    // ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        // SizedBox(width: width*0.055,),
                      ],
                    )
                  ],
                ),
              ),
            ),
            Positioned(
              height: 200,
              width: 290,
              top: 65,
              left: 30,
              child: Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("assets/kcard.jpg"),
                        fit: BoxFit.fill),
                    //color: Colors.black,
                    borderRadius: BorderRadius.all(Radius.circular(20))),
              ),
            ),
            Positioned(
              height: 250,
              width: 350,
              top: 260,
              // right: 5,
              left: 5,
              //bottom: 5,
              child: Container(
                //color: Colors.black,
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 100,
                      width: 275,
                      child: Row(
                        mainAxisAlignment: prefix0.MainAxisAlignment.center,
                        children: <Widget>[
                          MyButton(
                            icon: Icons.add_circle_outline,
                            Title: "Add a Card",
                          ),
                          prefix0.SizedBox(
                            width: 30,
                          ),
                          MyButton(
                            icon: Icons.delete_outline,
                            Title: "Del a Card",
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: 100,
                      width: 275,
                      child: Row(
                        mainAxisAlignment: prefix0.MainAxisAlignment.center,
                        children: <Widget>[
                          MyButton(
                            icon: Icons.list,
                            Title: "Cards List",
                          ),
                          prefix0.SizedBox(
                            width: 30,
                          ),
                          MyButton(
                            icon: Icons.more_horiz,
                            Title: "More",
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class Clipper08 extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final Path path = Path();
    path.lineTo(0.0, size.height);
    var End = Offset(size.width / 2, size.height - 30.0);
    var Control = Offset(size.width / 4, size.height - 50.0);

    path.quadraticBezierTo(Control.dx, Control.dy, End.dx, End.dy);
    var End2 = Offset(size.width, size.height - 80.0);
    var Control2 = Offset(size.width * .75, size.height - 10.0);

    path.quadraticBezierTo(Control2.dx, Control2.dy, End2.dx, End2.dy);
    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) {
    // TODO: implement shouldReclip
    return true;
  }
}
